package com.example.bubu.todolist;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;


import com.example.bubu.todolist.communicators.Communicator;

import java.util.LinkedList;


public class TaskListActivity extends AppCompatActivity implements Communicator {
    private DayTaskManager manager;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Входящие");
        setContentView(R.layout.activity_task_list);
        setNavigationDrawer();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerToggle = setupDrawerToggle();
        drawerLayout.addDrawerListener(drawerToggle);


        setSupportActionBar(toolbar);

        FrameLayout fragmentLayout = (FrameLayout)findViewById(R.id.fragmentAddTask);

        manager = DayTaskManager.getTaskManager(getApplicationContext());


        if (fragmentLayout != null) {
            if (savedInstanceState != null) {
                return;
            }

            StartWindowFragment fragment = new StartWindowFragment();
            SingleListFragment tasksListFragment = new SingleListFragment();
            LeftNavigationFragment navigationFragment = new LeftNavigationFragment();

            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();

            transaction.add(R.id.fragmentAddTask, fragment, "add_fragment");
            transaction.add(R.id.fragmentTasksList, tasksListFragment, "tasks_list_fragment");
            transaction.commit();
        }
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setNavigationDrawer() {

        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);
        return true;
    }

    @Override
    public void respond(String data) {
        System.out.println("My event dispatched here with " + data);

        Task task = new Task(data);
        manager.addTask(task);
        manager.save();

        LinkedList<Task> tasks = manager.getAllTasks();

        SingleListFragment tasksListFragment = (SingleListFragment) getFragmentManager().findFragmentByTag("tasks_list_fragment");
        tasksListFragment.getMyAdapter().refreshTasks(manager.getAllTasks());
    }
}