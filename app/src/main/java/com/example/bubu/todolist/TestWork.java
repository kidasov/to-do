package com.example.bubu.todolist;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

/**
 * Created by bubu on 29.05.17.
 * class created to test model functionality
 */

public class TestWork {
    public static void serialize(LinkedList<Day> days) {
        try {
            FileOutputStream fos = new FileOutputStream("myfile");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(days);
            oos.close();
            fos.close();
        } catch (Exception ex) {

        }
    }

    public static LinkedList<Day> deserialize() {
        LinkedList<Day> days = null;

        try {
            FileInputStream fis = new FileInputStream("myfile");
            ObjectInputStream ois = new ObjectInputStream(fis);
            days = (LinkedList<Day>)ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception ex) {

        }

        return days;
    }

    public static void main(String [] args) {
        System.out.println("Hello my little friend. What's wrong with you?");

        LinkedList<Day> days = new LinkedList<Day>();

        Day monday = new Day();
        monday.addTask("Read books");
        monday.addTask("Clean the room");

        days.add(monday);

        Day tuesday = new Day();
        tuesday.addTask("Help my parents");
        tuesday.addTask("Cook the dinner");

        days.add(tuesday);

        serialize(days);

        LinkedList<Day> days2 = deserialize();
        System.out.println(days2);

    }
}
