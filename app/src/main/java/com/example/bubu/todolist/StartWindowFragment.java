package com.example.bubu.todolist;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.bubu.todolist.communicators.Communicator;

/**
 * Created by bubu on 31.05.17.
 */

public class StartWindowFragment extends Fragment {
    Communicator communicator;
    EditText taskBodyEditText;
    Button showAddPanelButton;
    Button addTaskButton;
    Button cancelAddButton;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.start_window_fragment, container, false);

        final RelativeLayout addTaskPanel = (RelativeLayout)view.findViewById(R.id.addTaskPanel);
        addTaskPanel.setVisibility(View.INVISIBLE);

        showAddPanelButton = (Button) view.findViewById(R.id.showAddPanelButton);
        showAddPanelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTaskPanel.setVisibility(View.VISIBLE);
                showAddPanelButton.setVisibility(View.INVISIBLE);
            }
        });


        taskBodyEditText = (EditText)view.findViewById(R.id.taskBodyEditText);

        addTaskButton = (Button)view.findViewById(R.id.addTaskButton);
        addTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String text = taskBodyEditText.getText().toString();
                communicator.respond(text);
            }
        });

        cancelAddButton = (Button)view.findViewById(R.id.cancelAddButton);
        cancelAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTaskPanel.setVisibility(View.INVISIBLE);
                showAddPanelButton.setVisibility(View.VISIBLE);
            }
        });


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        System.out.println("on attach");

        if (context instanceof Activity) {
            Activity act = (Activity)context;
            communicator = (Communicator)act;
        }
    }
}
