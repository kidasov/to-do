package com.example.bubu.todolist;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bubu on 14.05.17.
 */

public class Task implements Serializable {
    private Date date;
    private String description;
    private boolean isOutDated;
    private boolean isDone;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isOutDated() {
        return isOutDated;
    }

    public void setOutDated(boolean outDated) {
        isOutDated = outDated;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Task(String description) {
        this.date = new Date();
        this.description = description;
        this.isDone = false;
        this.isOutDated = false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.description);
        sb.append(" ");
        sb.append(this.isDone);

        return sb.toString();
    }
}
