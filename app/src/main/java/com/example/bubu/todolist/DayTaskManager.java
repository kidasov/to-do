package com.example.bubu.todolist;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by bubu on 29.05.17.
 */

public class DayTaskManager implements Serializable {
    private LinkedList<Task> tasks;
    private static final String DAYS_FILE = "tasks";
    private Context context;
    private static DayTaskManager manager;

    public static DayTaskManager getTaskManager(Context ctx) {
        if (manager == null) {
            manager = new DayTaskManager(ctx);
        }

        return manager;
    }

    private DayTaskManager(Context ctx) {
        this.context = ctx;

        this.tasks = readFromFile(DAYS_FILE);
        if (this.tasks == null) {
            this.tasks = new LinkedList<Task>();
        }
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void serialize(String fileName) {
        try {
            FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(tasks);
            oos.close();
            fos.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

   private LinkedList<Task> deserialize(String fileName) {
        LinkedList<Task> tasks = null;

        try {
            FileInputStream fis = context.openFileInput(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            tasks = (LinkedList<Task>)ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception ex) {

        }

        return tasks;
    }

    public void removeTask(String task) {
        for (Iterator<Task> iterator = tasks.iterator(); iterator.hasNext();) {
            Task elementTask = iterator.next();
            if (task.equals(elementTask.getDescription())) {
                iterator.remove();
            }
        }
    }

    public void removeAll() {
        tasks.clear();
        this.save();
    }

    public LinkedList<Task> getAllTasks() {
        return tasks;
    }

    private void fillWithTestData() {
        tasks = new LinkedList<Task>();

        tasks.add(new Task("Read books"));
        tasks.add(new Task("Clean the room"));

        tasks.add(new Task("Help my parents"));
        tasks.add(new Task("Cook the dinner"));

        this.serialize(DAYS_FILE);

        LinkedList<Task> tasks2 = this.deserialize(DAYS_FILE);

//        System.out.println("Days2 " + days2);
    }

    public LinkedList<Task> readFromFile(String fileName) {
        return deserialize(fileName);
    }
    private void writeDaysToFile(String fileName) {
        serialize(fileName);
    }

    public void save() {
        writeDaysToFile(DAYS_FILE);
    }

}
