package com.example.bubu.todolist;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.bubu.todolist.adapters.TaskListAdapter;
import com.example.bubu.todolist.communicators.Communicator;

import java.util.LinkedList;

/**
 * Created by bubu on 14.05.17.
 */

public class SingleListFragment extends ListFragment {
    private Context context;
    private TaskListAdapter myAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Context c = getActivity().getApplicationContext();

        DayTaskManager manager = DayTaskManager.getTaskManager(getActivity().getApplicationContext());

        LinkedList<Task> tasks = manager.getAllTasks();

//        manager.removeAll();

        myAdapter = new TaskListAdapter(getActivity(), tasks);
        setListAdapter(myAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public TaskListAdapter getMyAdapter() {
        return myAdapter;
    }

}
