package com.example.bubu.todolist;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by bubu on 14.05.17.
 */

public class Day implements Serializable{
    private LinkedList<Task> tasks;

    public Day() {
        tasks = new LinkedList<Task>();
    }

    public void addTask(String strTask) {
        if (strTask != null && !strTask.isEmpty()) {
            Task task = new Task(strTask);
            tasks.add(task);
        }
    }

    public LinkedList<Task> getTasks() {
        return tasks;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Task task: tasks) {
            sb.append(task);
            sb.append("\n");
        }

        return sb.toString();
    }


}
