package com.example.bubu.todolist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.bubu.todolist.Day;
import com.example.bubu.todolist.DayTaskManager;
import com.example.bubu.todolist.R;
import com.example.bubu.todolist.Task;

import java.util.LinkedList;

/**
 * Created by bubu on 04.06.17.
 */

public class TaskListAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    LinkedList<Task> tasks;

    public TaskListAdapter(Context context, LinkedList<Task> tasks) {
        this.context = context;
        this.tasks = tasks;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.tasks.size();
    }

    @Override
    public Object getItem(int position) {
        return this.tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);
        }

        final Task task = getTask(position);


        RadioButton taskIsDoneRadioButton = (RadioButton)view.findViewById(R.id.taskIsDoneRadioButton);
        taskIsDoneRadioButton.setChecked(task.isDone());

        taskIsDoneRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DayTaskManager manager = DayTaskManager.getTaskManager(null);
                manager.removeTask(task.getDescription());
                manager.save();

              //  tasks.remove(position);
                TaskListAdapter.this.notifyDataSetChanged();
            }
        });

        TextView taskBodyTextView = (TextView)view.findViewById(R.id.taskBodyTextView);
        taskBodyTextView.setText(task.getDescription());

        return view;
    }

    Task getTask(int position) {
        return (Task)getItem(position);
    }

    public void refreshTasks(LinkedList<Task> tasks) {
        /*this.tasks.clear();
        this.tasks.addAll(tasks);*/
        notifyDataSetChanged();
    }
}
