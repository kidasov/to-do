package com.example.bubu.todolist.communicators;

/**
 * Created by bubu on 04.06.17.
 */

public interface Communicator {
    public void respond(String data);
}
